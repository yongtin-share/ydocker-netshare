
describe package("docker-volume-netshare") do
  it { should be_installed}
  its('version') { should eq "0.16" }
end
