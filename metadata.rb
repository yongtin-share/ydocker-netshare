name 'ydocker-netshare'
maintainer 'Tsang Yong'
maintainer_email 'tsang.yong@gmail.com'
license 'all_rights'
description 'Installs/Configures ydocker-netshare'
long_description 'Installs/Configures ydocker-netshare'
version '0.18.1'

depends "ydocker"
