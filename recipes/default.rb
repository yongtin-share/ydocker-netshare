#
# Cookbook Name:: ydocker-netshare
# Recipe:: default
#
# Copyright (c) 2016 Tsang Yong, All Rights Reserved.

directory "/packages"

remote_file "/packages/docker-volume-netshare.deb" do
  source node["netshare"]["package_url"]
  owner "root"
  group "root"
  mode "0644"
  action :create
end

dpkg_package "docker-volume-netshare" do
  source "/packages/docker-volume-netshare.deb"
  action :install
end

service "docker-volume-netshare" do
  action [ :start, :enable ]
end

